﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;                          
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//                                                        Java                 |                C#                  
//                                                  ---------------------------|-----------------------------
//                                                         import              |                using
//                                                         final               |               const
//                                                        boolean              |                bool
//                                                       createImage           |              new Bitmap
//                                                    picture.getGraphics()    |       Graphics.FromImage(picture)

namespace Applet
{
    public partial class Form1 : Form                                       // all the variables are declared
    {   
        private const int MAX = 256;                                        // max iterations
        private const double SX = -2.025;                                   // start value real
        private const double SY = -1.125;                                   // start value imaginary
        private const double EX = 0.6;                                      // end value real
        private const double EY = 1.125;                                    // end value imaginary
        private static int x1, y1, xs, xe, ys, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;   // zoom featuress    
        private static bool action, rectangle, finished, cycle, maxj; // mouseDragged bool is flag to replace built in Java function MouseDragged
        private static float xy;
        private Bitmap picture;                                             // bitmap of the fractal. (private Image picture)
        private Graphics g1;
        private Cursor c1, c2;
        private HSBColor HSBcol = new HSBColor();                           // HSBColor provided by DJM converts RGB colors to HSB
        private Pen p;                                                      // Pen used to draw lines to bitmap replaces Java canvas set color
        private ToolStripMenuItem selitem;                                  // static required to store sender object in a variable when moving between forms
        private static int j = 0;                                           // Slow animation and colour cycle

          
        public Form1()
        {
            InitializeComponent();
            init();
            start();
        }


        public void init()                                                  // all instances will be prepared
        {
                                                                            // setSize(640, 480);
            this.Width = 640;                                               // sets a width value
            this.Height = 480;                                              // sets a height value
            finished = false;                                               // Used later on for the if(finished) statement
                                                                            // addMouseListener(this); --- replaced with C# mouse controls
                                                                            // addMouseMotionListener(this); --- replaced with C# mouse controls
            c1 = Cursors.WaitCursor;                                        // c1 = new Cursor(Cursor.WAIT_CURSOR); // sets the mouse icon to a waiting hour
            c2 = Cursors.Cross;                                             // c2 = new Cursor(Cursor.CROSSHAIR_CURSOR); // Sets the mouse icon for when selecting area on picture
            x1 = this.Width;                                                // x1 = getSize().width;
            y1 = this.Height;                                               // y1 = getSize().height;
            xy = (float)x1 / (float)y1;
            picture = new Bitmap(x1, y1);                                   // picture = createImage(x1, y1);
            g1 = Graphics.FromImage(picture);                               // g1 = picture.getGraphics();
                                                                            //finished = true; --  moved to public void Form1_MouseDown
        }

         public void destroy()                                              // delete all instances 
         {
             if (finished)
             {
             // removeMouseListener(this); --- replaced with C# mouse controls
             // removeMouseMotionListener(this); --- replaced with C# mouse controls
              picture = null;
              g1 = null;
              c1 = null;
              c2 = null;
              System.GC.Collect();                                          // System.gc(); ---  garbage collection
             }
         }
         
        public void start()
        {
            action = false;                                                  // Action and Rectangle used for future IF statements
            rectangle = false;
            initvalues();                                                    // Run method initvalues()
            xzoom = (xende - xstart) / (double)x1;                           // Set zoom co-ordinates
            yzoom = (yende - ystart) / (double)y1;                           // Set zoom co-ordinates
            mandelbrot();                                                    // Run mandelbrot method
        }

        public void stop()                                                   // End the method running
        {
        }


        public void paint(Graphics g)
        {
            update(g);
        }

        public void update(Graphics g)
        {
            g.DrawImage(picture, 0, 0);                                      // g.drawImage(picture, 0, 0, this);
            if (rectangle)
            {
                p = new Pen(Color.White);                                    // g.setColor(Color.white);
                if (xs < xe)
                {
                    if (ys < ye) g.DrawRectangle(p, xs, ys, (xe - xs), (ye - ys));
                    else g.DrawRectangle(p, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g.DrawRectangle(p, xe, ys, (xs - xe), (ye - ys));
                    else g.DrawRectangle(p, xe, ye, (xs - xe), (ys - ye));
                }
            }
        }
        
        private void mandelbrot()                                           // calculate all points
        {
            int x, y;                                                       // Set values for the attributes
            float h, b, alt = 0.0f;                                         // that will be used to determine the points
            action = false;                                                 // on the image and the colours to be used
       
            // SetCursor(c1); --- replaced with Cursor.Current = this.c2; and moved to Form1_MouseUp
            // showStatus("Mandelbrot-Set will be produced - please wait..."); --- 
            //replaced with toolStripStatusLabel1.Text and moved to Form1_MouseDown

            Color colour = Color.Black;                                     // set a colour to black
            Pen myPen = new Pen(colour);                                    // creates myPen and sets the colour
            
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h;                                   // brightnes
                        HSBcol = new HSBColor(h, 0.8f, b);                  // calls the HSB class and calculates colour from HSB.cs and stores it in colour
                        Color myColor;
                        myColor = HSBcol.FromHSB(HSBcol);
                        myPen = new Pen(myColor);                           // uses myPen and sets the colour
                        alt = h;
                    }
                    g1.DrawLine(myPen, x, y, x + 1, y);                     // draws the fractal
                }
                // showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse."); --- replaced with toolStripStatusLabel1.Text and moved to Form1_MouseUp
                // setCursor(c2); --- replaced with Cursor.Current = this.c2; and moved to Form1_MouseMove
         
            action = true;
        }

        private float pointcolour(double xwert, double ywert)               // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = J;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues()                                           // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        public void Form1_Paint(object sender, PaintEventArgs e)            // draws rectangle to Form1
        {
            paint(e.Graphics);
        }

        // This is the mouse button is released and sets the variables values
        // for the XY coordiantes and for the zoom related variables

        public void Form1_MouseUp(object sender, MouseEventArgs e)              // when mouse is released
        {
            toolStripStatusLabel1.Text = "Mandelbrot-Set ready - please select zoom area with pressed mouse.";
            Cursor.Current = this.c1;
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                Refresh();                                                      //repaint();
                finished = false;                                               //Without this, it will rectangle loop
            }
        }


        public void Form1_MousePressed(object sender, MouseEventArgs e)         // This is the code that sets when the mouse button is pressed
        {
                toolStripStatusLabel1.Text = "Mandelbrot-Set will be produced - please wait...";
                if (action)
                {
                    xs = e.X;
                    ys = e.Y;
                    finished = true;                                            // Zoom box effect when selecting an area
            }
        }


        public void Form1_MouseMove(object sender, MouseEventArgs e)           // Method for when the mouse is moved around the screen this generates the values for the Zoom box rectangle
        {
            Cursor.Current = this.c2;
            if (finished == true)
            {
                if (action)
                {
                    xe = e.X;
                    ye = e.Y;
                    rectangle = true;                                           // without this, no show rectangle when hold click
                    Refresh();                                                  // without this, no show rectangle when hold click
                }
            }
        }



        //*******************************************************************************************************************************************************************//

        private void SaveImage_Click(object sender, EventArgs e)                // Saves image 
        {

            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filename = @"\Applet";
            string filetype = @".jpeg";
            int num = 1;

            while (File.Exists(path + filename + " " + num.ToString() + filetype))
            {
                num++;
            }

            picture.Save(path + filename + " " + num.ToString() + filetype);
            MessageBox.Show("Saved To Desktop");

        }


        private void saveState_Click(object sender, EventArgs e)                // Save State - save the four variables required by mandlebrot to draw fractal in array and write array to file
        {

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Filter = "Text Documents (*.txt)|*.txt";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.FileName = "SaveState_1.txt";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                String extension = Path.GetExtension(saveFileDialog1.FileName);
                String filename;
                switch (extension.ToLower())
                {
                    case ".txt":
                        filename = saveFileDialog1.FileName;
                        break;
                    default:
                        filename = saveFileDialog1.FileName + ".txt";
                        break;
                }

                string lines = "xstart=" + xstart + "\r\nystart=" + ystart + "\r\nxzoom=" + xzoom + "\r\nyzoom=" + yzoom;

                System.IO.StreamWriter file = new System.IO.StreamWriter(filename);
                file.WriteLine(lines);
                file.Close();
            }
        }


        private void exportToolStripMenuItem_Click(object sender, EventArgs e)              // Export (Save Dialogue)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();                            // Create new save file dialog object
            saveFileDialog.Filter = "JPEG Image|*.jpg|Bitmap Image|*.bmp|GIF Image|*.gif";   // Specify file types
            saveFileDialog.Title = "Save Fractal Image File";                                // Give it a Title
            saveFileDialog.ShowDialog();                                                     // Display
            if (saveFileDialog.FileName != "")                                               // Check filename is not empty and ready to save
            {
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog.OpenFile();   // Saves the Image through a FileStream
                switch (saveFileDialog.FilterIndex)                                          // Save image depending on file type selected by user
                {
                    case 1:
                        picture.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 2:
                        picture.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                        break;

                    case 3:
                        picture.Save(fs, System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }
                fs.Close();                                                                  // Close the file stream
            }
        }

        public int J                                                                        // Change color
        {
            get
            {
                return j;
            }
            set
            {
                j = value;
            }
        }

        public void checkItem(object sender)                                             // Checks a single item in the tool strip menu
        {
            
            if (selitem == null)
            {
            }
            else
            {
                selitem.Checked = false;
            }

            if (cycle == true)
            {
                selitem = colourCycleToolStripMenuItem;
            }
            else if (J == 0)
            {
                redToolStripMenuItem.Checked = true;
                selitem = yellowToolStripMenuItem;
            }
            else if (J == 35)
            {
                yellowToolStripMenuItem.Checked = true;
                selitem = yellowToolStripMenuItem;
            }
            else if (J == 70)
            {
                greenToolStripMenuItem.Checked = true;
                selitem = greenToolStripMenuItem;
            }
            else if (J == 120)
            {
                blueToolStripMenuItem.Checked = true;
                selitem = blueToolStripMenuItem;
            }
            else if (J == 160)
            {
                darkBlueToolStripMenuItem.Checked = true;
                selitem = darkBlueToolStripMenuItem;
            }
            else if (J == 180)
            {
                indigoToolStripMenuItem.Checked = true;
                selitem = indigoToolStripMenuItem;
            }
            else if (J == 200)
            {
                violetToolStripMenuItem.Checked = true;
                selitem = violetToolStripMenuItem;
            }
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            J = 0;
            cycle = false;
            mandelbrot();
            Invalidate(); 
            checkItem(sender);
        }

        private void yellowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            J = 35;
            cycle = false;
            mandelbrot();
            Invalidate(); 
            checkItem(sender);
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            J = 70;
            cycle = false;
            mandelbrot();
            Invalidate(); 
            checkItem(sender);
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            J = 120;
            cycle = false;
            mandelbrot();
            Invalidate(); 
            checkItem(sender);
        }

        private void darkBlueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            J = 160;
            cycle = false;
            mandelbrot();
            Invalidate(); 
            checkItem(sender);
        }

        private void indigoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            J = 180;
            cycle = false;
            mandelbrot();
            Invalidate(); 
            checkItem(sender);
        }

        private void violetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            J = 200;
            cycle = false;
            mandelbrot();
            Invalidate(); 
            checkItem(sender);
        }



        private Bitmap animation(Bitmap bitmap)                                   // Animation - converts bitmap fractal to GIF and pushes the palete entries pointer along one
        {
            MemoryStream stream = new MemoryStream();
            bitmap.Save(stream, ImageFormat.Gif);
            Bitmap colorBitmap = new Bitmap(stream);
            ColorPalette palette = colorBitmap.Palette;

            for (int i = 0; i < (palette.Entries.Length - 5); i++)
            {
                Color color = palette.Entries[i];
                palette.Entries[i] = palette.Entries[i + 1];
                palette.Entries[i + 1] = color;
            }
            colorBitmap.Palette = palette;
            stream.Close();
            return colorBitmap;
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)   // [Animation - start] menu option starts timer2 tick method which runs the method at set intervals
        {
            startToolStripMenuItem.Checked = true;
            timer2.Start();
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)    // [ Animation - stop] menu option stops the timer2 tick method running
        {
            startToolStripMenuItem.Checked = false;
            timer2.Stop();
            slowToolStripMenuItem.Checked = false;
            timer1.Stop();
        }

        private void slowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cycle = true;
            timer1.Start();
            checkItem(sender);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            mandelbrot();
            paletteShift();
            Invalidate();
        }

        public void paletteShift()                                  // Variable for colour cycle
        {
            MemoryStream stream = new MemoryStream();

            if (maxj == false && J < 200)
            {
                J = J + 2;
            }
            else if (maxj == false && J == 200)
            {
                maxj = true;
                J = J - 2;
            }
            else if (maxj == true && J > 0)
            {
                J = J - 2;
            }
            else if (maxj == true && J == 0)
            {
                maxj = false;
                J = J + 2;
            }
            stream.Close();
        }

        private void timer2_Tick(object sender, EventArgs e)    // at set intervals create a new image and assign animation color fractal picture and update graphics to display
        {
            Image image = animation(picture);
            g1 = Graphics.FromImage(picture);
            g1.DrawImage(image, 0, 0);
            Refresh();
        }

    }
}

